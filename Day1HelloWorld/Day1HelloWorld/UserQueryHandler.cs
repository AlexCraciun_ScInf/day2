﻿
using System;
using Day1HelloWorld.Interfaces;
using Day1HelloWorld.Exceptions;

namespace Day1HelloWorld
{
    class UserQueryHandler : IUserQueryHandler
    {
        public string ObtainQueryResponse(string input)
        {
            if (input.Equals("quit", StringComparison.InvariantCultureIgnoreCase))
            {
                throw new ProgramCloseException();
            }

            return $"Processed {input} ..., no commands implemented";
        }

      

    }

    class CoolQueryHandler : IUserQueryHandler
    {
        public string ObtainQueryResponse(string input)
        {
            return "Cool " + input;
        }

        private void DOSomething()
        {

        }
    }
}
