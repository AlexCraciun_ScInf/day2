﻿namespace Day1HelloWorld.Interfaces
{
    interface IUserQueryHandler
    {
        string ObtainQueryResponse(string input);
    }
}
