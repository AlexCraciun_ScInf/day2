﻿namespace Day1HelloWorld.Extra
{
    struct SampleStruct
    {
        private int Id { get; set; }

        public string Name { get; set; }

        public string GetRepresentation()
        {
            return this.Id.ToString() + Name;
        }
    }

    class SampleClass
    {
        private int Id { get; set; }

        public string Name { get; set; }

        public string GetRepresentation()
        {
            return this.Id.ToString() + Name;
        }

        public string CoolName => GetRepresentation().Substring(5, 6);

        public SampleClass()
        {
            Id = 1;
        }
    }
}
