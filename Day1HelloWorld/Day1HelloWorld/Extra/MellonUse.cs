﻿using System;

namespace Day1HelloWorld.Extra
{
    class MellonUse
    {
        public void MellonShifting()
        {
            Watermellon normalMellon = new Watermellon();
            normalMellon.Color = "yellow";

            SquareWattermellon square = new SquareWattermellon();
            square.Color = "green";

            square.Height = 5;
            square.Width = 10;

            Watermellon sqInDisquise = square;
            Console.WriteLine(sqInDisquise.Color);

            SquareWattermellon actualSquare = (SquareWattermellon) sqInDisquise;
            //Console.WriteLine(actualSquare.Width);

            //tryes to convert but returns null if conversion failed.
            var aactualSquare = sqInDisquise as SquareWattermellon;
            if(actualSquare != null)
            {
                Console.WriteLine(actualSquare.Width);
            }
            // Will not compile
            //sqInDisquise.Height = 10;
        }
    }
}
