﻿namespace Day1HelloWorld.Extra
{
    class Watermellon
    {
        public int Weight;

        public string Color;

        public bool Sweet;
        
        public string Description()
        {
            return "This " + Color + " mellon " + (Sweet ? "is" : "is not") + " sweet";
        } 
    }
}
