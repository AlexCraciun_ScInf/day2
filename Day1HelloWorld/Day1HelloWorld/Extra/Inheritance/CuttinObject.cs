﻿namespace Day1HelloWorld.Extra.Inheritance
{
    class CuttinObject
    {
        /// <summary>
        /// Gets or sets a numeric value indicating the sharpness
        /// </summary>
        public int Sharpness { get; set; }

        public bool IsSharp { get { return Sharpness > 50; } }

        public CuttinObject(int sharpQ)
        {
            this.Sharpness = sharpQ;
        }
        
        //Something should go here.
    }
}
