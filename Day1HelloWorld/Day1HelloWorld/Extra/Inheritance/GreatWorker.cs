﻿using System;

namespace Day1HelloWorld.Extra.Inheritance
{
   
    class ExperWorker : IGreatWorker
    {
        public void DoBasicWork()
        {
            Console.WriteLine("Basic stuff");
        }

        public void DoAdvancedWork()
        {
            DoBasicWork();
        }

        //But really, not so expert on work :D
        public void DoExpertWork()
        {
            DoAdvancedWork();
        }

        public void DoAmazingWork()
        {
            Think();
            DoExpertWork();
        }

        public void Think()
        {
            Console.WriteLine("First I think....");
        }
    }
}
