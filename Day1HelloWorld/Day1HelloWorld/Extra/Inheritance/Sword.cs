﻿namespace Day1HelloWorld.Extra.Inheritance
{
    class Sword : Blade
    {
        public bool HasGuard { get; set; }

        public Sword(int sharpQ, int length, bool hasGuard) : base(sharpQ, length)
        {
            this.HasGuard = HasGuard;
        }

        //This does not compile :(
        //public Sword()
        //{

        //}
    }
}
