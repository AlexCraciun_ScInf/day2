﻿using System;

namespace Day1HelloWorld.Extra.Inheritance.Hiding
{
    class BaseHidingClass
    {
        //Base protected methods can inherited
        protected void ProtectedOp()
        {
            Console.WriteLine("Base protected");
        }

        //Virtual methods can be inherited and overridden.
        protected void VirtualBaseOp()
        {
            Console.WriteLine("Virtual protected");
        }
    }

    class SimpleHidingClass : BaseHidingClass
    {

    }

    class HigherVirtualClass : BaseHidingClass
    {
        //We override the virtual method
        protected void VirtualBaseOp()
        {
            //base.VirtualBaseOp(); //We can use the old version.
            ProtectedOp(); //We can call the protected one
            Console.WriteLine("Higher virtual");
        }
    }
}
