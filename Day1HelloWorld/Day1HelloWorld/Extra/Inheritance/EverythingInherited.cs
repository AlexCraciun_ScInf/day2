﻿namespace Day1HelloWorld.Extra.Inheritance
{
    public class EverythingInherited
    {
        public class SecretiveClass
        {
            private int Secret { get; set; }

            public SecretiveClass()
            {
                this.Secret = 1;
            }
        }

        public class OpenClass : SecretiveClass
        {
            public int TrySecret()
            {
                var methodInfo = typeof(SecretiveClass).GetProperty("Secret",
                    System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

                // var result = methodInfo.GetValue(this, 0).ToString();
                // return int.Parse(result);
                return 0;
            }
        }
    }
}
