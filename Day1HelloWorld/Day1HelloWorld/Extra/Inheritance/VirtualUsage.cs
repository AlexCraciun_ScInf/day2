﻿using System;

namespace Day1HelloWorld.Extra.Inheritance.Virtual
{
    class BaseVirtualClass
    {
        //Base protected methods can inherited
        protected void ProtectedOp()
        {
            Console.WriteLine("Base protected");
        }

        //Virtual methods can be inherited and overridden.
        protected virtual void VirtualBaseOp()
        {
            Console.WriteLine("Virtual protected");
        }
    }

    class SimpleVirtualClass : BaseVirtualClass
    {

    }

    class HigherVirtualClass : BaseVirtualClass
    {
        //We override the virtual method
        protected override void VirtualBaseOp()
        {
            //base.VirtualBaseOp(); //We can use the old version.
            ProtectedOp(); //We can call the protected one
            Console.WriteLine("Higher virtual");
        }

        protected void ProtectedOp()
        {
            base.ProtectedOp();
            Console.WriteLine("Hiding Base protected");
        }
    }
}
