﻿using AdditionalLibrary;

namespace Day1HelloWorld.Extra.Inheritance
{
    class AmModifierImport: BaseAMClass { 

        public int NotSoSecret
        {
            get
            {
                return this.PublicSecret 
                    + this.ProtectedSecret 
                    + this.InternalSecret; 
                //No private secret
            }
        }

    }
}
