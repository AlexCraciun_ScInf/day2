﻿namespace Day1HelloWorld.Extra.Inheritance
{
    interface IBaseWorker
    {
        void DoBasicWork();
    }

    interface IAdvancedWorker : IBaseWorker
    {
        void DoAdvancedWork();
    }

    interface IExpertWorker: IAdvancedWorker
    {
        void DoExpertWork();
    }

    interface IThinker
    {
        void Think();
    }

    interface IGreatWorker: IExpertWorker, IThinker
    {
        void DoAmazingWork();
    }
}
