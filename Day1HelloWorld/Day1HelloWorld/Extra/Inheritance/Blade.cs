﻿
namespace Day1HelloWorld.Extra.Inheritance
{
    class Blade : CuttinObject
    {

        public int Length { get; set; }
    
        //You can use the base constructor, but do you need to?
        public Blade(int sharpQ,  int length) : base(sharpQ)
        {

        }
    }
}
