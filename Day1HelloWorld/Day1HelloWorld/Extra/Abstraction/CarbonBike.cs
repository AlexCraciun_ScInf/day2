﻿namespace Day1HelloWorld.Extra.Abstraction
{
    class CarbonBike : PrototypeBike
    {
        public CarbonBike() : base()
        {
            this.Speed = 2;
        }

        public override int Speed
        {
            get; protected set;
        }

        protected override void Rust()
        {
           //Do nothing, this is a cool carbon bike. It does not rust.
        }
    }
}
