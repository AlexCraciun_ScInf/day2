﻿using System;

namespace Day1HelloWorld.Extra.Abstraction
{
    partial class Car
    {
        public Car(int speed)
        {
            this.Speed = speed;
        }

        public void Go()
        {
            Console.WriteLine($"I am going at {Speed} km/h");
        }
    }
}
