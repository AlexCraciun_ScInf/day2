﻿using System;

namespace Day1HelloWorld.Extra.Abstraction
{
    class StaticHolder
    {
        public static int InstanceCount { get; private set; }
        private const string AnnounceFormat =  "There are {0} of us.";


        static StaticHolder()
        {
            //initializes instance counting from 0;
            InstanceCount = 0;
        }

        public StaticHolder()
        {
            //Increases the instance on every constructor call.
            InstanceCount++;
        }

        //Prints the number of instances.
        public static void PrintCount()
        {
            Console.WriteLine(string.Format(AnnounceFormat, InstanceCount));
        }
    }
}
