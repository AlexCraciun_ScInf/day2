﻿namespace Day1HelloWorld.Extra.Abstraction
{
    abstract class PrototypeBike
    {
        //An access modifier can be used on get or set to alter one of the operations.
        public int NumberOfWheels { get; private set; }

        public PrototypeBike()
        {
            NumberOfWheels = 2;
        }

        //Must be implemented
        public abstract int Speed { get; protected set; }

        protected abstract void Rust();
    }
}
