﻿using System;

namespace Day1HelloWorld.Extra.Abstraction
{
    public static class Utilities
    {
        private static string dateFormat;

        //Does not compile
        //public Utilities() { }

        static Utilities()
        {
            dateFormat = "dd-MM-yyyy";
        }
       

        public static string FormatDate(DateTime date)
        {
            return date.ToString(dateFormat);
        }
    }
}
