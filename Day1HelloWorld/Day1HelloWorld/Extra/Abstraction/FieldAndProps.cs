﻿using System;

namespace Day1HelloWorld.Extra.Abstraction
{
    class FieldAndProps
    {
        public int FielData;

        private int _propData;

        public int PropData
        {
            get
            {
                return _propData;
            }
            set
            {
                _propData = value;
            }
        }

        public int ReadOnlyPropData
        {
            get
            {
                return _propData;
            }
        }

        public int WriteOnlyPropData
        {
            set
            {
                _propData = value;
            }
        }
        
        //Standard best practice way to expose fields.
        public int AutoProp { get; set; }     

        public int ReadOnlyAutoProp { get; }

        //Does not compile. Props must have getter.
        //public int WriteOnlyAutoProp { set; }

        private int _logicPropetry;
        private int _accessCounter = 0;

        //Better if you have logic hidden behind it.
        public int LogicProperty
        {
            get
            {
                _accessCounter++;
                return _logicPropetry;
            }

            set
            {
                if (value <= 0)
                    throw new ArgumentException("LogicProperty must be positive");
                _logicPropetry = value;
            }
        }
    }
}
