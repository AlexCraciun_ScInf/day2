﻿
namespace Day1HelloWorld.Extra.Abstraction
{
    //Will not compile as this class is essentially useless.
    //Elements in a namespace can be internal, protected, protected internal or public
    //private class UselessClass { }

    internal class ExtendedAccessModifiers : AccessModifiers
    {
        public ExtendedAccessModifiers()
        {
            //Does not compile
            //this.HiddenField = 1;

            this.InheritableProp = 1;
            InheritInAssemblyProp = 1;

            //Does not compile
            //HiddenClass tryInstance;

            AdditionalLibrary.CanUseOutSide importedClass = new AdditionalLibrary.CanUseOutSide();
            importedClass.VisibleProp = 1;

            //Does not compile
            //importedClass.AssemblyOnlyProp = 2;
        }
    }
}
