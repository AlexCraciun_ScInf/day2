﻿//Namespaces are public by default and do not support access modifiers
namespace Day1HelloWorld.Extra.Abstraction
{
    //Class is visible anywhere.
    public class AccessModifiers
    {
        // Only in class.
        private int HiddenField;

        // In class and anywhere outside
        public int PublicProp { get; set; }

        //Default is private
        int NormalProp;

        //Internal, as in only in the Day1HelloWorld assembly.
        internal int AnotherNormalProp { get; set; }

        //Visible inside and only on classes that inherit this one
        protected int InheritableProp { get; set; }

        //Visible only to classes that inherit AccessModifiers OR are part of the same assembly.
        protected internal int InheritInAssemblyProp { get; set; }

        //Is not visible anywhere except within AccessModifiers class
        private class HiddenClass
        {
        }
        
        //Is visible in the Day1HelloWorld assembly.
        class AssemblyVisibleClass
        {
        }

        //Only classes that inherit from AccessModifiers class can use this class
        protected class InheritableClass
        {
        }
    }
}
