﻿namespace Day1HelloWorld.Extra.Abstraction
{
    internal class InternalUseClass
    {
    }

    

    public class PublicUsageClass
    {
        //Will generate errors as these operations can be accessed from outside the assembly

        //public void SpecialOp(InternalUseClass param) { }


        //protected InternalUseClass SpecialOp2(int param) { return null; }
        
    }
}
