﻿using System;
namespace Day1HelloWorld.Extra.Polymorhism.Abstract
{
    abstract class Animal
    {
        public string name;
        public abstract void Play();
    }

    class Dog : Animal
    {
        public string trick;
        public override void Play()
        {
            Console.WriteLine(name + " playing " + trick);
        }
    }

    class Cat : Animal
    {
        public string toy;
        public override void Play()
        {
            Console.WriteLine(name + " playing with " + toy);
        }

        public virtual void Meow()
        {
            Console.WriteLine("Cat meow");
        }
    }

    class SilentCat :Cat
    {
        public override void Play()
        {
            Console.WriteLine("Play very silently");
        }

        public override void Meow()
        {
            Console.WriteLine("Silent meow");
        }
    }
}
