﻿using System;

namespace Day1HelloWorld.Extra.Polymorhism.VirtualExample
{
    class Animal
    {
        public string name;
        public virtual void Play()
        {
            Console.WriteLine("rolling...");
        }
    }

    class Dog : Animal
    {
        private string trick;
        public override void Play()
        {
            Console.WriteLine(name + " playing " + trick);
        }

        public void Play(string toy)
        {
            Console.WriteLine(name + " playing with " + toy);
        }

        public void Play(string toy, int number)
        {
            Console.WriteLine(name + " playing with " + number + "of " + toy);
        }
    }
}
