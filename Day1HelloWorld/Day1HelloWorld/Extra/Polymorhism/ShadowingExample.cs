﻿using System;

namespace Day1HelloWorld.Extra.Polymorhism
{
    class Car
    {
        public virtual void Go()
        {
            Console.WriteLine("Car is go go going...");
        }

        public void Stop()
        {
            Console.WriteLine("Car is stopping");
        }
    }

    class FastCar : Car
    {
        public override void Go()
        {
            Console.WriteLine("Car is going fast...");
        }

        public new void Stop()
        {
            Console.WriteLine("Fast car is stopping");
        }
    }

    public static class CarTryOuts
    {
        public static void TryOut()
        {
            FastCar fast = new FastCar();
            fast.Go();
            fast.Stop();

            Car hiddenFast = fast;
            hiddenFast.Go();
            hiddenFast.Stop();
        }
    }
}
