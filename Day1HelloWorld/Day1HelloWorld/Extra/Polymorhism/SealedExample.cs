﻿namespace Day1HelloWorld.Extra.Polymorhism
{
    sealed class SealedExample
    {
    }

    //error
    class SealedTryHard //: SealedExample
    {

    }
}
