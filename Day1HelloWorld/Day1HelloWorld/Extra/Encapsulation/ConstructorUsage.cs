﻿namespace Day1HelloWorld.Extra.Encapsulation
{
    class ConstructorUsage
    {
        private void CreateSomething()
        {
            var item = new CarConstructorSample(150) { Speed = 130 };
        }
    }
}
