﻿using System;

namespace Day1HelloWorld.Extra.Encapsulation
{
    //One class can contain other classes or interfaces, though this is NOT a good practice.
    class MultipleImplementation
    {
        public interface ISimpleCalculator
        {
            int Add(int a, int b);
            int Divide(int a, int b);
        }

        public interface IComplexCalculator
        {
            int Add(int a, int b);
            int Divide(int a, int b);
        }

        class Calculator : ISimpleCalculator, IComplexCalculator
        {
            public int Add(int a, int b)
            {
                return a + b;
            }

            int ISimpleCalculator.Divide(int a, int b)
            {
                return a / b;
            }

            int IComplexCalculator.Divide(int a, int b)
            {
                if (b == 0) throw new DivideByZeroException("Bad idea");
                return a / b;
            }
        }
    }
}
