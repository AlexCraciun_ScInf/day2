﻿using System.Collections.Generic;
using System.Linq;

namespace Day1HelloWorld.Extra.Encapsulation
{
    class StudentClass : IStudentClass
    {
        private readonly List<Student> Students;

        public string ClassName { get; private set; }

        public int StudentNumber { get; private set; }

        public StudentClass(string name)
        {
            ClassName = name;
            StudentNumber = 0;
            Students = new List<Student>();
        }

        public bool EnrollStudent(string name)
        {
            if (this.Students.Any(s => s.Name == name)) return false;

            this.Students.Add(new Student(name, this));
            return true;
        }

        public bool EnrollStudent(string name, int initialWork)
        {
            if (this.Students.Any(s => s.Name == name)) return false;

            this.Students.Add(new Student(name, this) { WorLoad = initialWork });
            return true;
        }

        public bool RemoveStudent(string name)
        {
            if (!this.Students.Any(s => s.Name == name)) return false;
            this.Students.RemoveAll(s => s.Name == name);
            return true;
        }

        public bool AssignWork(string name, int work)
        {
            if (!this.Students.Any(s => s.Name == name)) return false;
            Students.FirstOrDefault(s => s.Name == name).WorLoad += work;
            return true;
        }
    }
}
