﻿using System;

namespace Day1HelloWorld.Extra.Encapsulation
{
    class ReadonlyUsageCar
    {
        public readonly DateTime CarManufactureDate = DateTime.Now;
        private readonly string ProducedName;
        private readonly string BrandName;

        public string Name { get; private set; }

        public ReadonlyUsageCar(string CarName)
        {
            this.ProducedName = CarName + "made on " + CarManufactureDate;
            CarManufactureDate = DateTime.UtcNow;
            this.Name = CarName;
        }

        public void TrySetBrand(string brand)
        {
            //Erroe
            //BrandName = brand;
        }
    }
}
