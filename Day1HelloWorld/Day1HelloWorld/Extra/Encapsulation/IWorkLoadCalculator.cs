﻿using System.Collections.Generic;

namespace Day1HelloWorld.Extra.Encapsulation
{
    interface IWorkLoadCalculator
    {
        int CalculateAverageWorkload(IEnumerable<Student> students);
    }
}
