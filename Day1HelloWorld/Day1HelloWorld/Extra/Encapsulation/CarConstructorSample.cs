﻿using System;

namespace Day1HelloWorld.Extra.Encapsulation
{
    class CarConstructorSample
    {

        private static DateTime firstUsedTime;
        public string Brand { get; private set; }
        public string Name { get; private set; }

        public int MaxSpeed { get; private set; }
        public int Speed { get; set; }

        static CarConstructorSample()
        {
            firstUsedTime = DateTime.Now;
        }

        //By Default, all classes have a no parameter constructor available

        //Writing any other constructor negates this
        public CarConstructorSample(string brand)
        {
            this.Brand = brand;
        }

        //Constructor overloading
        public CarConstructorSample(string brand, string name)
        {
            this.Brand = brand;
            this.Name = name;
        }

        //Constructor chaining is possible
        public CarConstructorSample(string brand, string name, int maxSpeed) : this(brand, name)
        {
            this.MaxSpeed = maxSpeed;
        }

        public bool IncompleteDefinitions { get; private set; }

        private CarConstructorSample(bool incompleteDefinition)
        {
            if (incompleteDefinition)
            {
                this.Name = "NoName";
                this.Brand = "NoManufacturer";
                this.IncompleteDefinitions = incompleteDefinition;
            }
        }

        //Innacessible from outside
        public CarConstructorSample(int maxSpeed) : this(true)
        {
            this.MaxSpeed = MaxSpeed;
        }

        //Copy constructor
        public CarConstructorSample(CarConstructorSample source)
        {
            this.Brand = source.Brand;
            this.Name = "Copy of " + source.Name;
            this.Speed = source.Speed;
        }
    }
}
