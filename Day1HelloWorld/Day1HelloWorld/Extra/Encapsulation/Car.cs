﻿using System;

namespace Day1HelloWorld.Extra.Encapsulation
{
    class Car : IVehicle
    {
        private const int MaxSpeed = 120;

        public bool IsSelfPropelled
        {
            get
            {
                return true;
            }
        }

        public int Wheels
        {
            get
            {
                return 4;
            }
        }

        public bool CanGoAtSpeed(int desiredSpeed)
        {
            return desiredSpeed > MaxSpeed;
        }

        public void Go()
        {
            throw new NotImplementedException();
        }
    }
}
