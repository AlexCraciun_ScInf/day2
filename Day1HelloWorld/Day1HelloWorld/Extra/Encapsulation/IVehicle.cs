﻿namespace Day1HelloWorld.Extra.Encapsulation
{
    interface IVehicle
    {
        int Wheels { get; }

        void Go();

        bool CanGoAtSpeed(int desiredSpeed);

        bool IsSelfPropelled { get; }
    }
}
