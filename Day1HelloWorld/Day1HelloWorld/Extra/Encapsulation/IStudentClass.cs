﻿namespace Day1HelloWorld.Extra.Encapsulation
{
    interface IStudentClass
    {
        string ClassName { get; }

        bool EnrollStudent(string name);

        bool EnrollStudent(string name, int initialWork);

        bool RemoveStudent(string name);

        bool AssignWork(string name, int work);
    }
}
