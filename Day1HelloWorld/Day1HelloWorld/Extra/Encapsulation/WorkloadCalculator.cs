﻿using System.Collections.Generic;
using System.Linq;

namespace Day1HelloWorld.Extra.Encapsulation
{
    class WorkloadCalculator : IWorkLoadCalculator
    {
        public int CalculateAverageWorkload(IEnumerable<Student> students)
        {
            if (students == null || !students.Any()) return 0;

            return (students.Sum(s => s.WorLoad)) / students.Count();
        }
    }
}
