﻿namespace Day1HelloWorld.Extra.Encapsulation
{
    class Student
    {
        public string Name { get; private set; }

        public int WorLoad { get; set; }

        public StudentClass MemberOfClass { get; private set; }

        public Student(string name, StudentClass participatingIn)
        {
            this.Name = name;
            this.MemberOfClass = participatingIn;
        }

        public string Speak()
        {
            return "My name is " + Name + " and I am enrolled in " + this.MemberOfClass.ClassName;
        }
    }
}
