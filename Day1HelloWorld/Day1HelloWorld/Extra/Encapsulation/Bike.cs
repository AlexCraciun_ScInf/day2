﻿using System;

namespace Day1HelloWorld.Extra.Encapsulation
{
    class Bike : IVehicle
    {
        
        public int RiderPower { get; private set; }
        
        public Bike(int riderPower)
        {
            RiderPower = riderPower;
        }

        bool IVehicle.IsSelfPropelled
        {
            get
            {
                return false;
            }
        }

        int IVehicle.Wheels
        {
            get
            {
                return 2;
            }
        }

        bool IVehicle.CanGoAtSpeed(int desiredSpeed)
        {
            return RiderPower * 10 > desiredSpeed;
        }

        void IVehicle.Go()
        {
            Console.WriteLine("Bike going");
        }
    }
}
