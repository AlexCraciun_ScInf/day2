﻿using System;

namespace Day1HelloWorld.Extra.Exceptions
{
    public static class SimpleException
    {
        public static void Do()
        {
            try
            {
                string s = Console.ReadLine();
                Int32.Parse(s);
                Console.WriteLine(
                "You entered valid Int32 number {0}.", s);
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid integer number!");
            }
            catch (OverflowException)
            {
                Console.WriteLine(
                "The number is too big to fit in Int32!");
            }
        }

        public static void DoMore()
        {
            try
            {
                int divideBy = 0;
                int divide = 15;
                var a = divide / divideBy;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message  + 
                    System.Environment.NewLine +  
                    ex.StackTrace + 
                    System.Environment.NewLine + 
                    ex.InnerException?.ToString());
            }
        }
    }
}
