﻿using System;

namespace Day1HelloWorld.Extra.Exceptions
{
    public static class ThrowExceptionExample
    {
        public static void Do()
        {
            try
            {

                string s = Console.ReadLine();
                try
                {
                    switch (s)
                    {
                        case "1":
                            throw new NullReferenceException("boom");
                        case "2":
                            throw new ArgumentException("bop!");
                        default:
                            throw new IndexOutOfRangeException("shwoo!");
                    }
                }
                catch (NullReferenceException ex)
                {
                    Console.WriteLine("1 " + ex.ToString());
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine("2 " + ex.ToString());
                }
                catch (Exception ex)
                {
                    Console.WriteLine("default " + ex.ToString());
                    throw new Exception("Not sure of exception");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(System.Environment.NewLine + "Caught outer " + ex.ToString());
            }
        }
    }
}

