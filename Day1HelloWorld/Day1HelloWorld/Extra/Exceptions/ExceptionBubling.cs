﻿using System;

namespace Day1HelloWorld.Extra.Exceptions
{
    public static class ExceptionBubling
    {
        class BoobyTrap
        {
            public void Explode()
            {
                throw new Exception("BOOM!");
            }
        }

        class Aparment
        {
            public void Enter()
            {
                BoobyTrap trap = new BoobyTrap();
                trap.Explode();
            }
        }

        class Building
        {
            public void Access()
            {
                Aparment ap = new Aparment();
                try
                {

                    ap.Enter();
                }
                catch (Exception ex)
                {
                    //Building is too weak to handle it :(
                    throw ex;
                }
            }
        }

        public static void Do()
        {
            var build = new Building();
            try
            {
                build.Access();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problem at building level " + ex.Message);
            }
        }
    }
}
