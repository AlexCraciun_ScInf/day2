﻿using System;

namespace Day1HelloWorld.Extra.Exceptions
{
    class MultipleExceptions
    {
        public static void Do()
        {
            try
            {
                string s = Console.ReadLine();
                Int32.Parse(s);
                Console.WriteLine(
                "You entered valid Int32 number {0}.", s);
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid integer number!");
            }
            catch (OverflowException)
            {
                Console.WriteLine(
                "The number is too big to fit in Int32!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Generic exception caught " + ex.Message);
            }
            finally
            {
                Console.WriteLine("Exception catching is over!");
            }
        }
    }
}
