﻿namespace Day1HelloWorld.Extra.SOLID
{
    class AdditionCalculator
    {
        public int Add(int a, int b)
        {
            return a + b;
        }

        //Don't add more things here
    }

    class ComplexCalculator : AdditionCalculator
    {

        public int Substract(int a, int b)
        {
            return a - b;
        }
    }

    //Use a static class
    static class CalculatorExtensionMethod
    {
        //And a static method.
        public static int Multiply(this ComplexCalculator calc, int a, int b)
        {
            //It will only have access to the public properties of ComplexCalculator
            return a * b;
        }
    }

    class User
    {
        void DoMath()
        {
            var calc = new ComplexCalculator();
            var a = calc.Add(1, 2);
            var b = calc.Substract(10, 5);
            var c = calc.Multiply(2, 3);
        }
    }
}
