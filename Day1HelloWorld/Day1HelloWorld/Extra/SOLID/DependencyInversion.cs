﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day1HelloWorld.Extra.SOLID
{
    interface IConsoleWriter
    {
        void WriteLine(string message);
    }

    class BasicWriter : IConsoleWriter
    {
        public void WriteLine(string message)
        {
            Console.WriteLine(message);
        }
    }

    class DateWriter : IConsoleWriter
    {
        public void WriteLine(string message)
        {
            Console.WriteLine(DateTime.Now.ToString() + " " + message);
        }
    }

    class WriterProvider
    {
        public IConsoleWriter GetWriter()
        {
            //This is the only part of the entire program that decides what writer to use
            return new BasicWriter();
            //or
            //return new DateWriter();
        }
    }

    class WriterService
    {
        private readonly IConsoleWriter innerWritter;

        public WriterService()
        {
            var provider = new WriterProvider();
            innerWritter = provider.GetWriter();
        }

        public void WriteBook()
        {
            innerWritter.WriteLine("Roses are read");
            innerWritter.WriteLine("My name's not Dave");
            innerWritter.WriteLine("This makes no sense");
            innerWritter.WriteLine("Microwave");
        }
    }
}
