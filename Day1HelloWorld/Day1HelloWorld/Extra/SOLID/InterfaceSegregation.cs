﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day1HelloWorld.Extra.SOLID
{
    public interface AddInteface
    {
        int Add(int a, int b);
    }

    public interface CalculatorInteface : AddInteface
    {
        int Subtract(int a, int b);
    }

    class CalculatorProvder
    {
        public AddInteface GetAddInterface()
        {
            //Provide some class that implements AddInteface
            return null;
        }

        public CalculatorInteface GetCalculatorInteface()
        {
            //Provide some class that implements CalculatorInteface

            //Changes in any class that implements CalculatorInteface will require this class to update 
            //the way it get's it's implementation.
            return null;
        }
    }

    class MathConsumer{
        void DoMath()
        {
            var provider = new CalculatorProvder();

            AddInteface mockAdd = provider.GetAddInterface();
            CalculatorInteface mockCalc = provider.GetCalculatorInteface();

            //Bad
            var result = mockCalc.Add(2, 3);

            //Good
            result = mockAdd.Add(2, 3);
        }
    }
}
