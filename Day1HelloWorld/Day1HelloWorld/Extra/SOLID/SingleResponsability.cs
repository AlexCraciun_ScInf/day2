﻿namespace Day1HelloWorld.Extra.SOLID
{
    class Worker
    {
        public void Work()
        {

        }

        public void GetPaid()
        {

        }
    }

    class ConfusedWorker{
        public void Work()
        {

        }

        //What does this have to do with a worker ?
        int Add(int a, int b)
        {
            return a + b;
        }
    }

}
