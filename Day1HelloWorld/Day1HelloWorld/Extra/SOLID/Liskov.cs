﻿using System;

namespace Day1HelloWorld.Extra.SOLID
{
    class Plane
    {
        public virtual void Fly()
        {
            Console.WriteLine("Fly");
        }
    }

    class Jet: Plane
    {
        public override void Fly()
        {
            Console.WriteLine("Fly fast");
        }
    }

    class Passenger
    {
        public void Fly()
        {
            Plane plane = new Plane();
            plane.Fly();

            //We can replace with a jet, and still fly, but we will fly faster;
            plane = new Jet();
            plane.Fly();
        }
    }
}
