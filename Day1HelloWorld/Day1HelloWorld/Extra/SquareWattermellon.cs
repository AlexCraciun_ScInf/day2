﻿namespace Day1HelloWorld.Extra
{
    class SquareWattermellon : Watermellon
    {
        public int Height;

        public int Width;

        public string Description()
        {
            return "This " + Color + " square mellon " + (Sweet ? "is" : "is not") + " sweet";
        }
    }

    class SuperMellon : SquareWattermellon
    {

    }
}
