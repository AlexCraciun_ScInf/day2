﻿using Day1HelloWorld.Exceptions;
using Day1HelloWorld.Extra;
using Day1HelloWorld.Extra.Abstraction;
using Day1HelloWorld.Interfaces;
using System;

namespace Day1HelloWorld
{
    class Program
    {
        private static IUserQueryHandler handler = new UserQueryHandler();

        static int Main(string[] args)
        {
            //((CoolQueryHandler)handler).ObtainQueryResponse(string.Empty);
            /*
            Watermellon mellon = new Watermellon();
            mellon.Color = "green";

            SquareWattermellon square = new SquareWattermellon();
            square.Color = "blue";

            SuperMellon super = new SuperMellon();

            Console.WriteLine(mellon.Description());
            Console.WriteLine(square.Description());
            Console.WriteLine(super.Description());
            */
            SampleClass x = new SampleClass();

            MellonUse shift = new MellonUse();
            shift.MellonShifting();

            Console.WriteLine("Input 'quit' to terminate.");
            Console.CancelKeyPress += new ConsoleCancelEventHandler(myHandler);

            try
            {
                while (true)
                {
                    Console.WriteLine("Awaiting input...");
                    string input = Console.ReadLine();
                    Console.WriteLine(handler.ObtainQueryResponse(input));
                }
            }
            catch (ProgramCloseException ex)
            {
                Console.WriteLine("Quit received");
                EndProgram();
                return 1;
            }
            catch
            {
                Console.WriteLine("Exception ocurred");
                EndProgram();
                return -1;
            }
        }

        protected static void myHandler(object sender, ConsoleCancelEventArgs args)
        {
            Console.WriteLine("Force termination command received");
            EndProgram();
        }

        private static void EndProgram()
        {
            Console.WriteLine("Press any key to close the console.");
            Console.ReadKey();
        }

        private static void WriteInt(int a)
        {
            Console.WriteLine(a);
            a = 11;
        }

        private static void WriteMyParam(MyParam something)
        {
            Console.WriteLine(something.value);
            something.value = 10;
        }
    }


    class MyParam
    {
        public int value;
    }
}
