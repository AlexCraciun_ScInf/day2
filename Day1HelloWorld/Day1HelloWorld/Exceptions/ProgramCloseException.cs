﻿using System;

namespace Day1HelloWorld.Exceptions
{
    class ProgramCloseException : ApplicationException
    {
        public ProgramCloseException()
        {
            this.SetHelp();
        }

        public ProgramCloseException(Exception innerException) : base("AppException", innerException)
        {
            SetHelp();
        }

        private void SetHelp()
        {
            this.HelpLink = "https://docs.microsoft.com/en-us/dotnet/api/system.applicationexception?view=netframework-4.8";
        }
    }
}
