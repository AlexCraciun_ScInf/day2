﻿

namespace AdditionalLibrary
{
   //Just because a file has a name, does not mean we need to make a class with the same name inside it.

    public class BaseAMClass
    {
        private int Secret { get; set; }

        public BaseAMClass()
        {
            this.Secret = 5;
        }

        protected int ProtectedSecret
        {
            get
            {
                return this.Secret;
            }
            set
            {
                this.Secret = value;
            }
        }

        protected internal int InternalSecret
        {
            get
            {
                return this.Secret;
            }
            set
            {
                this.Secret = value;
            }
        }

        public int PublicSecret
        {
            get
            {
                return this.Secret;
            }
            set
            {
                this.Secret = value;
            }
        }
    }
}
